from staging.basic import Log
from staging.gitAPI import GitAPI
from staging.dockerBackend import Docker
from staging.configFile import config
from staging.basic import setUpStage, Stage, fillStages

import BaseHTTPServer
import json
import ConfigParser
import os
from git.exc import InvalidGitRepositoryError
from git.exc import NoSuchPathError
from git import Repo
import urllib2

logger = Log(__name__)

class HTTPServer:
    def __init__(self):
        from staging.gitAPI import ProjectNotFoundError
        host = config.get("server", 'host')
        if host == "''":
            host = ''
        try:
            port = int(config.get("server", "port"))
        except Exception as e:
            port = 9000
        gitPath = config.get("git", "repos") + "/" + config.get("gitlab", "project_name")
        try:
            repo = Repo(gitPath)
            assert repo.bare
            repo.remotes["origin"]
            api = GitAPI()
            GitAPI.projId = api.getProjectIdByName(config.get("gitlab", "project_name"))
        except NoSuchPathError as e:
            print("path to localRepo %s not found" % gitPath)
            return
        except InvalidGitRepositoryError as e:
            print("%s is not a valid git repo" % gitPath)
            return
        except AssertionError as e:
            print("repo is not bare")
            return
        except IndexError as e:
            print("remote origin not found")
            return
        except ProjectNotFoundError as e:
            print("Project %s not found" % config.get("gitlab", "project_name"))
            return
        server_address = (host, port)
        fillStages()
        import signal

        def signal_handler(signal, frame):
            from staging.basic import writeStages
            writeStages()
            print("Done exiting now")
            os._exit(0)

        signal.signal(signal.SIGINT, signal_handler)
        signal.signal(signal.SIGTERM, signal_handler)

        httpd = BaseHTTPServer.HTTPServer(server_address, MyRequestHandler)
        try:
            httpd.serve_forever()
        except Exception as e:
            print("Main thread")
            print(str(e))
            sys.exit(0)


class MyRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_POST(self):
        message = 'OK'
        self.send_response(200)
        self.send_header("Content-Type", "text")
        self.send_header("Content-length", str(len(message)))
        self.end_headers()
        self.wfile.write(message)
        logger.logInfo("Processing new request")
        api = GitAPI()
        length = int(self.headers['Content-Length'])
        data = self.rfile.read(length)
        logger.logDebug("received request from %s with body %s" % (self.client_address[0], data))
        dataJson = json.loads(data)
        ref = dataJson["ref"].split("/")[2]
        owner = dataJson["user_name"]
        try:
            data = api.getFile(ref, ".staging.ini")
        except urllib2.HTTPError as e:
            logger.logInfo("No staging file found in this branch %s" % ref)
            return

        f = open("/tmp/staging.ini", "w")
        f.write(data)
        f.close()

        f = open("/tmp/staging.ini")
        config2 = ConfigParser.ConfigParser()
        config2.readfp(f)


        if not config2.has_section("stage"):
            return

        try:
            name = config2.get("stage", "name")
            port = config2.get("stage", "port")
        except ConfigParser.NoOptionError as e:
            return

        if not os.path.isdir(config.get("git", "trees") + "/" + name):
            os.mkdir(config.get("git", "trees") + "/" + name)

        gitPath = config.get("git", "repos") + "/" + config.get("gitlab", "project_name")
        api.resetCommit(gitPath, config.get("git", "trees") + "/" + name, ref)
        stage = setUpStage(name, port, config.get("git", "trees") + "/" + name, ref, owner)
        print(stage)

        api.commentOnCommit(ref, "Your stage is ready at http://" + config.get("stage", "host") + ":" + str(stage.port))
