from staging.dockerbackend import Docker, NameConflictError, PortConflictError, NoImageError

class Stage(object):
    """
        This class represents a stage in the development environment
    """

    # a list containing all current stages
    stages = []

    def __init__(self, docker, name, port, work_tree, image, env=None):
        if not type(docker) == Docker:
            raise Error("Stage Contsructor must use docker as instance of Docker class")
        self.docker = docker
        self.name = name
        self.port = port
        self.work_tree = work_tree
        self.image = image
        self.env = env

    def create_stage(self):
        """
            create_stage: creates a new stage and creates a docker container for it
            then adds it to the stages list
        """
        try:
            self.container = self.docker.createContainer(self.name, self.port, self.work_tree, self.image, self.env)
            self.docker.startContainer(self.name)
            if self.port == None:
                self.port = self.docker.getPort(self.name)
            Stage.stages.append(self)
        except NameConflictError as e:
            # maybe stage already exists we must make sure that its port is the same as the port here
            try:
                port = self.docker.getPort(self.name)
            except Exception as e:
                port = 0
            if port != self.port:
                self.remove_stage()
                self.create_stage()
        except PortConflictError as e:
            self.port = None
            self.create_stage()

        except NoImageError as e:
            raise

    def remove_stage(self):
        try:
            self.docker.removeContainer(self.name)
        except Exception as e:
            pass

        try:
            Stage.stages.remove(self)
        except ValueError as e:
            for stage in Stage.stages:
                if self.name == stage.name:
                    Stage.stages.remove(stage)

    def __eq__(self, stage):
        return self.name == stage.name and self.port == stage.port and self.work_tree == stage.work_tree \
                and self.image == stage.image and self.env == stage.env

    def exists(self):
        for stage in Stage.stages:
            if(self == stage):
                return True
        return False

    def getStages(self):
        return Stage.stages
