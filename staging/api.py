from staging.basic import Log
import BaseHTTPServer
import threading
import sys
from staging.basic import stages
import json

logger = Log(__name__)

requests = ["/stages"]

class NotImplementedError(Exception):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)



class APIRequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):

    def sendResponse(self, code, message):
        self.send_response(code)
        self.send_header("Content-Type", "json")
        message = json.dumps(message)
        self.send_header("Content-length", str(len(message)))
        self.end_headers()
        self.wfile.write(message)

    def getMethodName(self, name):
        return name.strip("/").replace("/", "_")

    def stages(self):
        response = []
        index = 0
        for stage in stages:
            from staging.dockerBackend import Docker
            docker = Docker()
            if docker.hasContainerWithName(stage.name):
                response.append({"name": stage.name, "port": stage.port, "owner": stage.owner, "workTree": stage.workTree})
        self.sendResponse(200, response)
    def do_GET(self):
        if self.path in requests:
            methodName = self.getMethodName(self.path)
            method = getattr(self, methodName)
            try:
                method()
            except AttributeError as e:
                self.sendResponse(404, "%s NOT implemented")


class API(threading.Thread):
    def __init__(self, port):
        threading.Thread.__init__(self)
        self.port = port

    def run(self):
        server_address = ("", self.port)
        httpd = BaseHTTPServer.HTTPServer(server_address, APIRequestHandler)
        try:
            httpd.serve_forever()
        except Exception as e:
            sys.exit(0)
