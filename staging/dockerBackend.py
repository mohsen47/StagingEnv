from staging.basic import Log, Stage, deleteStage
from staging.configFile import config

from docker.client import Client
from docker.errors import APIError, NotFound
from docker import utils
import re

logger = Log(__name__)

class Docker:
    def __init__(self):
        self.cli = Client(base_url=config.get("docker", "socket"))

    def createContainer(self, image, name, port, volume):
        myEnv = utils.parse_env_file(config.get("env", "path"))
        try:
            logger.logDebug("trying to create container with name %s" % name)
            container = self.cli.create_container(image=image, detach=True, ports=[80], volumes=["/var/www/html"],
            name=name, environment=myEnv,
            host_config=self.cli.create_host_config(port_bindings={
                80:port
            },
            binds={
            volume: {
                'bind': '/var/www/html',
                'mode': 'rw'
            }
            })
            )
            if port is not None:
                logger.logInfo("created container with image %s and name %s and port %d and volume %s" % (image, name, int(port), volume))
            else:
                logger.logInfo("created container with image %s and name %s and volume %s" % (image, name, volume))
            return container
        except APIError as e:
            p = re.search('409 Client Error: Conflict', str(e))
            if p:
                raise NameConflictError
            else:
                logger.logCritical("NAMECONFLICT " + str(e))
        except NotFound as e:
            d.pull("nimmis/apache-php5")
            try:
                logger.logDebug("trying to create container with name %s" % name)
                container = self.cli.create_container(image=image, detach=True, ports=[80], volumes=["/var/www/html"],
                name=name, environment=myEnv,
                host_config=self.cli.create_host_config(port_bindings={
                    80:port
                },
                binds={
                volume: {
                    'bind': '/var/www/html',
                    'mode': 'rw'
                }
                })
                )
                if port is not None:
                    logger.logInfo("created container with image %s and name %s and port %d and volume %s" % (image, name, int(port), volume))
                else:
                    logger.logInfo("created container with image %s and name %s and volume %s" % (image, name, volume))
                return container
            except APIError as e:
                p = re.search('409 Client Error: Conflict', str(e))
                if p:
                    raise NameConflictError
                else:
                    logger.logCritical("NAMECONFLICT " + str(e))


    def startContainer(self, container, name):
        try:
            self.cli.start(container=container.get('Id'))
            logger.logInfo("started container with name %s" % name)
        except APIError as e:
            if re.search("failed: port is already allocated", str(e)):
                raise PortConflictError
            else:
                logger.logCritical("PORTCONFLICT " + str(e))


    def stopContainer(self, name):
        self.cli.stop(name)
        logger.logInfo("stopped container with name %s" % name)
    def removeContainer(self, name):
        self.cli.remove_container(name)
        logger.logInfo("removed container with name %s" % name)

    def run(self, image, name, port, volume, branch, owner):
        stage = Stage(name, int(port), volume, branch, owner)
        try:
            container = self.createContainer(image, name, port, volume)
        except NameConflictError as e:
            self.stopContainer(name)
            self.removeContainer(name)
            container = self.createContainer(image, name, port, volume)
            deleteStage(stage)
        try:
            self.startContainer(container, name)
        except PortConflictError as e:
            self.stopContainer(name)
            self.removeContainer(name)
            container = self.createContainer(image, stage.name, None, volume)
            self.startContainer(container, stage.name)
            stage.port = int(self.cli.port(name, 80)[0]["HostPort"])
        return stage

    def hasContainerWithName(self, name):
        try:
            self.cli.inspect_container(name)
            return True
        except NotFound as e:
            return False

class NameConflictError(APIError):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

class PortConflictError(APIError):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)
