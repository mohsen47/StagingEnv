from docker.client import Client
from docker.errors import APIError, NotFound
from docker import utils
import re

class Docker(Client):
    """
        A class to create and delete containers
    """
    def __init__(self, base_url = "unix://var/run/docker.sock"):
        Client.__init__(self, base_url, 'auto')

    def createContainer(self, name, port, work_tree, image, env=None):
        """
            create a container using the supplied arguments and return it
            Params:
            * name: name of the container
            * port: the host port exposed to inside the container
            * work_tree: the path to the source code tree in the host mounted inside the container
            * image: the docker image used to create the container
            * env: The environment variables passed to the container when it will be ran
            Returns (dict): A dictionary with an image 'Id' key and a 'Warnings' key.
        """
        try:
            container = self.create_container(image=image, detach=True, ports=[80], volumes="/var/www/html",
                name=name, environment=env, host_config = self.create_host_config(
                    port_bindings = {
                        80:port
                    },
                    binds = {
                     work_tree: {
                        'bind': '/var/www/html',
                        'mode': 'rw'
                     }
                    }
                ))
            return container
        except APIError as e:
            p = re.search('409 Client Error: Conflict', str(e))
            if p:
                raise NameConflictError()
            else:
                p = re.search('404 Client Error: Not Found', str(e))
                if p:
                    raise NoImageError('cannot find image ' + image)
                else:
                    raise Exception("unkown error: " + str(e))
    def startContainer(self, name):
        try:
            self.start(name)
        except APIError as e:
            if re.search("bind: address already in use", str(e)):
                raise PortConflictError("port already in use")
            else:
                raise

    def removeContainer(self, name):
        try:
            self.remove_container(name)
        except APIError as e:
            if re.search("409 Client Error: Conflict", str(e)):
                self.stopContainer(name)
                self.removeContainer(name)
            else:
                raise

    def stopContainer(self, name):
        try:
            self.stop(name)
        except NotFound as e:
            if re.search('404 Client Error: Not Found', str(e)):
                return
            else:
                raise

    def getPort(self, name):
        try:
            return int(self.port(name, 80)[0]["HostPort"])
        except NotFound as e:
            raise NoContainerError("container %s not found" % (name))
        except TypeError as e:
            raise NoContainerError("container %s created but not running" % (name))

    def hasContainer(self, name):
        try:
            self.inspect_container(name)
            return True
        except NotFound as e:
            return False

    def listContainers(self, all):
        return self.containers(all=all)

class NameConflictError(APIError):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

class PortConflictError(APIError):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

class NoImageError(APIError):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

class NoContainerError(APIError):
    def __init__(self, *args, **kwargs):
        Exception.__init__(self, *args, **kwargs)
