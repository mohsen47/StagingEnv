from staging.basic import Log
from staging.configFile import config

from git import Repo
import urllib2
import requests
import json
import os
import re

logger = Log(__name__)

class ProjectNotFoundError(Exception):
    def __init__(self,*args,**kwargs):
        Exception.__init__(self,*args,**kwargs)

class GitAPI:
    def __init__(self):
        self.server = config.get("gitlab", "server")
        self.access_token = config.get("gitlab", "access_token")

        self.http = urllib2

        logger.logInfo("Created API instance for server '%s'" % self.server)

    def sendAPICall(self, path):
        logger.logInfo("Sending this api request '%s' to server '%s'" % (path, self.server))
        if not "?" in path:
            connection = self.http.urlopen("https://" + self.server + "/api/v3" + path +"?private_token=" + self.access_token + "&per_page=100")
        else:
            connection = self.http.urlopen("https://" + self.server + "/api/v3" + path +"&private_token=" + self.access_token + "&per_page=100")
        return connection.read()

    def getProjectIdByName(self, name):
        path = "/projects"
        data = self.sendAPICall(path)
        jsonData = json.loads(data)
        for item in jsonData:
            if item["name"] == name:
                return item["id"]

        raise ProjectNotFoundError()

    def getFile(self, ref, name):
        path = "/projects/" + str(GitAPI.projId) + "/repository/blobs/" + str(ref) + "?filepath=" + str(name)
        try:
            data = self.sendAPICall(path)
        except urllib2.HTTPError as e:
            p = re.search("HTTP Error 404: Not Found", str(e))
            if p:
    		    raise
            else:
                logger.logCritical("Uknow git api error " + str(e))
                raise
        return data

    def commentOnCommit(self, commit, comment):
        path = "/projects/" + str(GitAPI.projId) + "/repository/commits/" + str(commit) + "/comments"
        params = {"note" : comment}
        headers = {"PRIVATE-TOKEN" : self.access_token}
        requests.post("https://" + self.server + "/api/v3" + path, headers=headers, params=params)

    def resetCommit(self, localRepo, workTree, ref):
        repo = Repo(localRepo)

        os.environ["GIT_DIR"] = localRepo
        os.environ["GIT_WORK_TREE"] = workTree

        git = repo.git
        git.fetch("origin")
        git.reset("--hard", "origin/" + ref)
        logger.logInfo("reset localRepo %s to branch %s and work tree %s" % (localRepo, ref, workTree))

GitAPI.projId = 0
