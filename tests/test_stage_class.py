import unittest

import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

from staging.Stage import Stage
from staging.dockerbackend import Docker

class TestStage(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestStage, self).__init__(*args, **kwargs)
        self.docker = Docker()

    def test_create_stage(self):
        # create a stage with a new name and a new port
        stage = Stage(self.docker, "test_stage", 12345, "/test", "ubuntu")
        stage.create_stage()

        self.assertTrue(stage.exists())
        self.assertTrue(self.docker.hasContainer("test_stage"))
        self.assertEqual(self.docker.getPort("test_stage"), 12345)

        # create a stage with an old name and a new port
        stage = Stage(self.docker, "test_stage", 1235, "/test", "ubuntu")
        stage.create_stage()

        self.assertTrue(stage.exists())
        self.assertTrue(self.docker.hasContainer("test_stage"))
        self.assertEqual(self.docker.getPort("test_stage"), 1235)

        # create a stage with an old name and the same port this should do nothing
        stage = Stage(self.docker, "test_stage", 1235, "/test", "ubuntu")
        stage.create_stage()

        self.assertTrue(stage.exists())
        self.assertTrue(self.docker.hasContainer("test_stage"))
        self.assertEqual(self.docker.getPort("test_stage"), 1235)

        # create a stage with a new name and old port, this should choose a new port randomly

        stage1 = Stage(self.docker, "test_stage1", 1234, "/test", "ubuntu")
        stage1.create_stage()

        self.assertTrue(stage1.exists())
        self.assertTrue(self.docker.hasContainer("test_stage1"))
        self.assertEqual(stage1.port, self.docker.getPort("test_stage1"))

        stage.remove_stage()
        stage1.remove_stage()

    def test_remove_stage(self):
        stage = Stage(self.docker, "test_stage", 12345, "/test", "ubuntu")
        stage1 = Stage(self.docker, "test_stage1", 12346, "/test", "ubuntu")
        stage.create_stage()

        self.assertTrue(stage.exists())
        self.assertTrue(self.docker.hasContainer("test_stage"))
        self.assertEqual(self.docker.getPort("test_stage"), 12345)

        stage.remove_stage()

        self.assertFalse(stage.exists())
        self.assertFalse(self.docker.hasContainer("test_stage"))

        self.assertFalse(stage1.exists())

        # test removing a stage that does not exist
        stage1.remove_stage()


    def test_getStages(self):
        stage1 = Stage(self.docker, "test_stage1", 12345, "/test", "ubuntu")
        stage2 = Stage(self.docker, "test_stage2", 12346, "/test", "ubuntu")
        stage = Stage(self.docker, "test_stage", 12347, "/test", "ubuntu")

        stage1.create_stage()
        stage2.create_stage()

        stages = stage1.getStages()

        self.assertEqual(len(stages), 2)
        self.assertIn(stage1, stages)
        self.assertIn(stage2, stages)
        self.assertNotIn(stage, stages)

        stage1.remove_stage()
        stage2.remove_stage()
